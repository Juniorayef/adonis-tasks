function showInput(id){
	var edit = document.querySelector('#edit-'+id);
	var form = document.querySelector('#edit-form-'+id);
	var formList = document.querySelectorAll('.form-list');
	var check = form.classList.contains('is-hidden');

	formList.forEach(function(item, index){
		item.classList.add("is-hidden");
	});
	form.classList.remove("is-hidden");
	if(check){
		form.classList.remove("is-hidden");
	}else{
		form.classList.add("is-hidden");
	}
}